from django.urls import include, path
from rest_framework.routers import DefaultRouter

from server.main.api.views.wallet import WalletModelView


router = DefaultRouter()
router.register(r'wallets', WalletModelView, basename='wallets')

urlpatterns = [
    path('', include(router.urls)),
]
