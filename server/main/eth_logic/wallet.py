import secrets

from django.conf import settings
from eth_account import Account
from hexbytes import HexBytes
from pydantic import BaseModel, Field
from web3 import Web3
from web3.types import Wei

from server.main.models import Wallet

w3 = Web3(Web3.HTTPProvider(settings.MAINNET_URL))


class RawTransaction(BaseModel):
    nonce: int
    to: str
    value: int
    gas: int
    gas_price: int = Field(..., alias='gasPrice')


def generate_wallet_credentials() -> tuple[str, str]:
    private_key = f'0x{secrets.token_hex(32)}'
    return private_key, Account.from_key(private_key).address


def get_wallet_balance(address: str) -> Wei:
    return w3.eth.get_balance(address)


def create_and_perform_transaction(from_wallet: str, to_wallet: str) -> HexBytes:
    from_address = Web3.toChecksumAddress(from_wallet)
    to_address = Web3.toChecksumAddress(to_wallet)
    gas_price = w3.eth.gas_price

    raw_transaction = RawTransaction(
        nonce=w3.eth.getTransactionCount(from_address),
        to=to_address,
        value=get_wallet_balance(from_wallet) - settings.GAS * gas_price,
        gas=settings.GAS,
        gasPrice=gas_price,
    )

    signed_transaction = w3.eth.account.signTransaction(
        transaction_dict=raw_transaction.dict(by_alias=True),
        private_key=Wallet.objects.get(address=from_wallet).private_key,
    )

    return w3.eth.sendRawTransaction(signed_transaction.rawTransaction)
