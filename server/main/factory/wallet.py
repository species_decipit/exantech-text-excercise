import secrets

from eth_account import Account
from factory import LazyAttribute, LazyFunction
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice

from server.main.models import Wallet, CurrencyChoice


class WalletFactory(DjangoModelFactory):
    private_key = LazyFunction(lambda: f'0x{secrets.token_hex(32)}')
    address = LazyAttribute(lambda wallet: Account.from_key(wallet.private_key).address)
    currency = FuzzyChoice(choices=CurrencyChoice.values)

    class Meta:
        model = Wallet
