from django.db.models import Model, TextChoices, CharField


class CurrencyChoice(TextChoices):
    ETH = 'eth', 'Ethereum'


class Wallet(Model):
    address = CharField(max_length=128)
    private_key = CharField(max_length=128)
    currency = CharField(choices=CurrencyChoice.choices, max_length=4)
