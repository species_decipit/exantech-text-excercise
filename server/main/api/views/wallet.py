from django.db.models import QuerySet
from drf_spectacular.utils import extend_schema_view, extend_schema
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.viewsets import GenericViewSet

from server.main.api.serializers.wallet import (
    WalletSerializer,
    TransferRequestSerializer,
    TransferResponseSerializer,
)
from server.main.eth_logic.wallet import create_and_perform_transaction
from server.main.models import Wallet


@extend_schema_view(
    transfer=extend_schema(
        request=TransferRequestSerializer,
        responses=TransferResponseSerializer,
    ),
)
class WalletModelView(CreateModelMixin, ListModelMixin, GenericViewSet):
    def get_queryset(self) -> QuerySet:
        return Wallet.objects.all()

    def get_serializer_class(self) -> type[Serializer]:
        if self.action in ('create', 'list'):
            return WalletSerializer
        elif self.action == 'transfer':
            return TransferRequestSerializer

    @action(methods=['POST'], detail=False)
    def transfer(self, request: Request) -> Response:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        hash_value = create_and_perform_transaction(
            from_wallet=serializer.validated_data['_from'],
            to_wallet=serializer.validated_data['_to'],
        )
        return Response(data=TransferResponseSerializer({'hash': hash_value}).data)
