from rest_framework.exceptions import ValidationError
from rest_framework.fields import ChoiceField, CharField
from rest_framework.serializers import ModelSerializer, Serializer

from server import settings
from server.main.eth_logic.wallet import generate_wallet_credentials, get_wallet_balance, w3
from server.main.models import Wallet, CurrencyChoice


class WalletSerializer(ModelSerializer):
    def create(self, validated_data) -> Wallet:
        private_key, address = generate_wallet_credentials()
        return Wallet.objects.create(
            private_key=private_key,
            address=address,
            currency=validated_data['currency'],
        )

    class Meta:
        model = Wallet
        fields = (
            'currency',
            'address',
        )
        extra_kwargs = {
            'address': {'read_only': True},
        }


class TransferRequestSerializer(Serializer):
    currency = ChoiceField(choices=CurrencyChoice.choices, required=True)
    _from = CharField(max_length=128, required=True)
    _to = CharField(max_length=128, required=True)

    @staticmethod
    def validate__from(from_address: str) -> str:
        if not Wallet.objects.filter(address=from_address).exists():
            raise ValidationError('From address is not presented in the service')
        if get_wallet_balance(from_address) < w3.eth.gas_price * settings.GAS:
            raise ValidationError('From address does not have ethereum to pay commission')
        return from_address


class TransferResponseSerializer(Serializer):
    hash = CharField(max_length=1024)
