from server.main.eth_logic.wallet import create_and_perform_transaction
from server.main.models import Wallet

create_and_perform_transaction(Wallet.objects.get(pk=3).address, Wallet.objects.get(pk=4).address)
