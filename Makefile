WORKDIR := $(shell pwd)/src

MANAGE_PY = python manage.py

export DJANGO_SETTINGS_MODULE=server.settings

runserver:
	FILE=.env
	if [ ! -f "$FILE" ]; then cp .env.template .env; fi
	@docker-compose up --build -d
	@docker exec python_eth python manage.py migrate

stopserver:
	@docker-compose down -v

test_from_host:
	@docker-compose run --rm python pytest tests