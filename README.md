# ETH Wallet (TEST EXANTECH EXERCISE)

### Prerequisites
 - Docker
 - Docker Compose
 - Make


### Run service
 - Run service in docker: `make runserver`
 - Now you can visit `localhost:8000`
 - API Documentation (ReDoc): `localhost:8000/api/docs`
 - Stop service: `make stopserver`


### Run tests
 - Run tests in docker: `make test_from_host`


### Other
 - [API Schema in YAML](api_schema.yaml)
 - [Views and Serializers](server/main/api)
 - [ETH Business Logic](server/main/eth_logic)
 - [Models](server/main/models.py)
 - [Tests](tests)