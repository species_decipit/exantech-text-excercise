import pytest
from pytest_lazyfixture import lazy_fixture

from server.main.factory.wallet import WalletFactory
from server.main.models import Wallet


@pytest.fixture()
def wallet() -> Wallet:
    return WalletFactory()


@pytest.fixture()
def another_wallet() -> Wallet:
    return WalletFactory()


@pytest.fixture(params=[lazy_fixture('wallet')])
def from_address(request) -> str:
    assert isinstance(request.param, Wallet)
    return request.param.address


@pytest.fixture(params=[lazy_fixture('another_wallet')])
def to_address(request) -> str:
    assert isinstance(request.param, Wallet)
    return request.param.address
