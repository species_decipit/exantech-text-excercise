import pytest
from rest_framework.test import APIClient


@pytest.fixture()
def api_client() -> APIClient:
    return APIClient()


@pytest.fixture()
def create_wallet_request() -> dict[str, str]:
    return {'currency': 'eth'}


@pytest.fixture()
def transfer_request(from_address: str, to_address: str) -> dict[str, str]:
    return {
        '_from': from_address,
        '_to': to_address,
        'currency': 'eth',
    }
