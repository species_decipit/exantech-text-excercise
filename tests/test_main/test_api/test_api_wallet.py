from unittest.mock import patch, MagicMock

import pytest
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.test import APIClient

from server.main.models import Wallet

pytestmark = [
    pytest.mark.django_db(transaction=True),
]


def test_create_wallet(api_client: APIClient, create_wallet_request: dict[str, str]) -> None:
    response = api_client.post('/api/wallets/', data=create_wallet_request)

    wallet = Wallet.objects.get()

    assert response.status_code == HTTP_201_CREATED
    assert wallet.private_key
    assert wallet.address


def test_list_wallets(api_client: APIClient, wallet: Wallet) -> None:
    response = api_client.get('/api/wallets/')

    wallet = Wallet.objects.get()

    assert response.status_code == HTTP_200_OK
    assert response.json() == [
        {
            'address': wallet.address,
            'currency': wallet.currency,
        },
    ]


def test_transfer_from_not_exist_wallet(
    api_client: APIClient,
    transfer_request: dict[str, str],
    wallet: Wallet,
) -> None:
    wallet.delete()

    response = api_client.post('/api/wallets/transfer/', data=transfer_request)

    assert response.status_code == HTTP_400_BAD_REQUEST
    assert response.json() == {'_from': ['From address is not presented in the service']}


def test_transfer_with_not_enough_eth(
    api_client: APIClient,
    transfer_request: dict[str, str],
    wallet: Wallet,
) -> None:
    response = api_client.post('/api/wallets/transfer/', data=transfer_request)

    assert response.status_code == HTTP_400_BAD_REQUEST
    assert response.json() == {'_from': ['From address does not have ethereum to pay commission']}


@patch(
    'server.main.eth_logic.wallet.w3.eth.sendRawTransaction',
    MagicMock(return_value='some hash'),
)
@patch('server.main.eth_logic.wallet.get_wallet_balance', MagicMock(return_value=10e100))
@patch('server.main.api.serializers.wallet.get_wallet_balance', MagicMock(return_value=10e100))
def test_transfer(
    api_client: APIClient,
    transfer_request: dict[str, str],
    wallet: Wallet,
) -> None:
    response = api_client.post('/api/wallets/transfer/', data=transfer_request)

    assert response.status_code == HTTP_200_OK
    assert response.json() == {'hash': 'some hash'}
