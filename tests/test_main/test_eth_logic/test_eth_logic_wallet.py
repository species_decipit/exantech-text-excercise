from unittest.mock import patch, MagicMock, call

import pytest
from web3 import Web3

from server.main.eth_logic.wallet import (
    generate_wallet_credentials,
    get_wallet_balance,
    create_and_perform_transaction,
)
from server.main.models import Wallet


def test_generate_wallet_credentials() -> None:
    private_key, address = generate_wallet_credentials()

    assert len(private_key) == 66
    assert len(address) == 42


def test_get_wallet_balance() -> None:
    _, address = generate_wallet_credentials()
    balance = get_wallet_balance(address)

    assert balance == 0


@pytest.mark.django_db(transaction=True)
@patch('server.main.eth_logic.wallet.w3.eth.account.signTransaction')
@patch(
    'server.main.eth_logic.wallet.w3.eth.sendRawTransaction',
    MagicMock(return_value='some hash'),
)
@patch('server.main.eth_logic.wallet.get_wallet_balance', MagicMock(return_value=10e100))
@patch('server.main.api.serializers.wallet.get_wallet_balance', MagicMock(return_value=10e100))
def test_create_and_perform_transaction(
    sign_transaction_patched: MagicMock,
    to_address: str,
    wallet: Wallet,
) -> None:
    hash_value = create_and_perform_transaction(wallet.address, to_address)

    assert hash_value == 'some hash'
    assert sign_transaction_patched.call_args == call(
        transaction_dict={
            'nonce': 0,
            'to': Web3.toChecksumAddress(to_address),
            'value': sign_transaction_patched.call_args[1]['transaction_dict']['value'],
            'gas': 21000,
            'gasPrice': sign_transaction_patched.call_args[1]['transaction_dict']['gasPrice'],
        },
        private_key=wallet.private_key,
    )
